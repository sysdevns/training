package uk.co.morrisonspls.sysdevns.logintrainingapplication.modules.login;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import uk.co.morrisonspls.sysdevns.logintrainingapplication.model.LoginRequest;
import uk.co.morrisonspls.sysdevns.logintrainingapplication.model.User;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

public class LoginPresenterTest {

    private static final String VALID_USER = "ABC";
    private static final String NULL_USER = null;
    private static final String EMPTY_STRING_USER = "";
    private static final String VALID_PASSWORD = "password";

    LoginPresenter.View view;
    LoginPresenter presenter;

    @Before
    public void setup() {
        view = mock(LoginPresenter.View.class);
        presenter = new LoginPresenter(view);
    }

    @Test
    public void whenUsernameIsValidThenValidUsernameIsTrue() {
        boolean result = presenter.validUsername(VALID_USER);
        Assert.assertEquals(true, result);
    }

    @Test
    public void whenUsernameIsNullThenValidUsernameIsFalse() {
        boolean result = presenter.validUsername(NULL_USER);
        Assert.assertEquals(false, result);
    }

    @Test
    public void whenUsernameIsEmptyStringThenValidUsernameIsFalse() {
        boolean result = presenter.validUsername(EMPTY_STRING_USER);
        Assert.assertEquals(false, result);
    }

    @Test
    public void whenDoLoginThenHideErrorIsCalled() {
        presenter.doLogin(new LoginRequest(VALID_USER, VALID_PASSWORD));
        verify(view).hideError();
    }

    @Test
    public void whenDoLoginThenShowLoginProgressIsCalled() {
        presenter.doLogin(new LoginRequest(VALID_USER, VALID_PASSWORD));
        verify(view).showLogonInProgress();
    }

    @Test
    public void whenOnSuccessThenLaunchDashboardIsCalled() {
        presenter.onSuccess(new User(null, null));
        verify(view).launchDashboard();
    }

    @Test
    public void whenOnErrorThenHideLoginProgressIsCalled() {
        presenter.onError((LoginPresenter.View.Messages) null);
        verify(view).hideLogonInProgress();
    }

    @Test
    public void whenOnErrorThenShowErrorIsCalled() {
        presenter.onError(LoginPresenter.View.Messages.CONNECTION_FAILED);
        verify(view).showError(LoginPresenter.View.Messages.CONNECTION_FAILED);
    }
}
