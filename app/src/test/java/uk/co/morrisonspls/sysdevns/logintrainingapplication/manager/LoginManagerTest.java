package uk.co.morrisonspls.sysdevns.logintrainingapplication.manager;

import org.junit.Before;
import org.junit.Test;

import java.net.HttpURLConnection;

import okhttp3.Headers;
import okhttp3.MediaType;
import okhttp3.ResponseBody;
import okio.BufferedSource;
import retrofit2.Call;
import retrofit2.Response;
import uk.co.morrisonspls.sysdevns.logintrainingapplication.model.LoginRequest;
import uk.co.morrisonspls.sysdevns.logintrainingapplication.model.User;
import uk.co.morrisonspls.sysdevns.logintrainingapplication.modules.login.LoginPresenter;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;


public class LoginManagerTest {

    User user;
    LoginRequest loginRequest;
    Call<User> call;
    LoginManager.LoginServiceListener loginServiceListener;
    LoginManager loginManager;

    @Before
    public void setUp() throws Exception {
        user = new User("token", "username");
        loginRequest = new LoginRequest("username", "password");
        call = mock(Call.class);
        loginServiceListener = mock(LoginManager.LoginServiceListener.class);
        loginManager = new LoginManager();
        loginManager.attemptLogin(loginRequest, loginServiceListener);
    }

    @Test
    public void whenLoginSuccessfulThenOnResponseInvokesOnSuccess() {
        Response<User> response = Response.success(user, new Headers.Builder().build());

        loginManager.onResponse(call, response);

        verify(loginServiceListener).onSuccess(user);
    }

    @Test
    public void whenLoginFailsAuthErrorThenOnResponseInvokesOnErrorWithInvalidCredentials() {
        Response<User> response = Response.error(HttpURLConnection.HTTP_UNAUTHORIZED, new ResponseBody() {
            @Override
            public MediaType contentType() {
                return null;
            }

            @Override
            public long contentLength() {
                return 0;
            }

            @Override
            public BufferedSource source() {
                return null;
            }
        });

        loginManager.onResponse(call, response);

        verify(loginServiceListener).onError(LoginPresenter.View.Messages.INVALID_CREDENTIALS);
    }

    @Test
    public void whenLoginFailsWithAnyOtherErrorThenOnResponseInvokesOnErrorWithBadResponse() {
        Response<User> response = Response.error(HttpURLConnection.HTTP_INTERNAL_ERROR, new ResponseBody() {
            @Override
            public MediaType contentType() {
                return null;
            }

            @Override
            public long contentLength() {
                return 0;
            }

            @Override
            public BufferedSource source() {
                return null;
            }
        });

        loginManager.onResponse(call, response);

        verify(loginServiceListener).onError(LoginPresenter.View.Messages.BAD_RESPONSE);
    }

    @Test
    public void whenOnFailureThenOnErrorCalledWithConnectionFailed() {
        loginManager.onFailure(call, new Throwable());

        verify(loginServiceListener).onError(LoginPresenter.View.Messages.CONNECTION_FAILED);
    }
}
