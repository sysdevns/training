package uk.co.morrisonspls.sysdevns.logintrainingapplication.modules.login;

import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import uk.co.morrisonspls.sysdevns.logintrainingapplication.R;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;

@RunWith(AndroidJUnit4.class)
public class LoginActivityUiTest {

    @Rule
    public ActivityTestRule<LoginActivity> activityRule = new ActivityTestRule<>(LoginActivity.class);

    @Test
    public void whenValidDataEnteredThenDashboardActivityLaunched() throws InterruptedException {

        onView(withId(R.id.et_user))
                .perform(typeText("sysdevns"));

        onView(withId(R.id.et_password))
                .perform(typeText("secret"));

        onView(withId(R.id.bt_log_in)).perform(click());

        onView(withId(R.id.activity_dashboard_acticity))
                .check(matches(isDisplayed()));
    }

}
