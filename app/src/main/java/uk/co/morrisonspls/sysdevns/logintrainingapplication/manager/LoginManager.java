package uk.co.morrisonspls.sysdevns.logintrainingapplication.manager;

import java.net.HttpURLConnection;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.morrisonspls.sysdevns.logintrainingapplication.api.LoginService;
import uk.co.morrisonspls.sysdevns.logintrainingapplication.model.LoginRequest;
import uk.co.morrisonspls.sysdevns.logintrainingapplication.model.User;
import uk.co.morrisonspls.sysdevns.logintrainingapplication.modules.login.LoginPresenter;

public class LoginManager implements Callback<User> {

    private final LoginService loginService;
    private LoginServiceListener loginServiceListener;

    public LoginManager() {
        this(ServiceGenerator.provideLoginService());
    }

    public LoginManager(LoginService loginService) {
        this.loginService = loginService;
    }

    public void attemptLogin(LoginRequest loginRequest, final LoginServiceListener loginServiceListener) {
        this.loginServiceListener = loginServiceListener;
        loginService.login(loginRequest).enqueue(this);
    }

    @Override
    public void onResponse(Call<User> call, Response<User> response) {
        if (response.isSuccessful()) {
            loginServiceListener.onSuccess(response.body());
        } else if (response.code() == HttpURLConnection.HTTP_UNAUTHORIZED) {
            loginServiceListener.onError(LoginPresenter.View.Messages.INVALID_CREDENTIALS);
        } else {
            loginServiceListener.onError(LoginPresenter.View.Messages.BAD_RESPONSE);
        }
    }

    @Override
    public void onFailure(Call<User> call, Throwable t) {
        loginServiceListener.onError(LoginPresenter.View.Messages.CONNECTION_FAILED);
    }

    public interface LoginServiceListener {
        void onSuccess(User user);

        void onError(LoginPresenter.View.Messages message);
    }
}
