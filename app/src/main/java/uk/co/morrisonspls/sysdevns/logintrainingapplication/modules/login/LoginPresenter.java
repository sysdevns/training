package uk.co.morrisonspls.sysdevns.logintrainingapplication.modules.login;

import uk.co.morrisonspls.sysdevns.logintrainingapplication.manager.LoginManager;
import uk.co.morrisonspls.sysdevns.logintrainingapplication.model.LoginRequest;
import uk.co.morrisonspls.sysdevns.logintrainingapplication.model.User;

public class LoginPresenter implements LoginManager.LoginServiceListener {

    private View loginView;

    public LoginPresenter(View loginActivity) {
        this.loginView = loginActivity;
    }

    public void loginButtonPressed(String username, String password) {
        if (validUsername(username)) {
            doLogin(new LoginRequest(username, password));
        } else {
            loginView.showError(View.Messages.MISSING_USERNAME);
        }
    }

    protected void doLogin(LoginRequest loginRequest) {
        loginView.hideError();
        loginView.showLogonInProgress();
        LoginManager loginManager = new LoginManager();
        loginManager.attemptLogin(loginRequest, this);
    }


    protected boolean validUsername(String user) {
        return (user != null && user.length() > 0);
    }

    @Override
    public void onSuccess(User user) {
        loginView.launchDashboard();
    }

    @Override
    public void onError(View.Messages message) {
        loginView.hideLogonInProgress();
        loginView.showError(message);
    }

    public interface View {

        void showError(Messages errorMsg);

        void hideError();

        void launchDashboard();

        void showLogonInProgress();

        void hideLogonInProgress();

        enum Messages {
            MISSING_USERNAME,       // R.string.login_missing_username
            INVALID_CREDENTIALS,    // R.string.error_invalid_credentials
            BAD_RESPONSE,           // R.string.login_bad_response
            CONNECTION_FAILED       // R.string.login_connection_failed
        }

    }

}
