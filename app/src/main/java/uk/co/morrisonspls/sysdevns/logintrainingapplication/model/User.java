package uk.co.morrisonspls.sysdevns.logintrainingapplication.model;

/**
 * Created by sysdevns on 27/10/2016.
 */

public class User {

    private String accessToken;
    private String username;

    public User(String accessToken, String username) {
        this.accessToken = accessToken;
        this.username = username;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public String getUsername() {
        return username;
    }

    @Override
    public String toString() {
        return "User{" +
                "accessToken='" + accessToken + '\'' +
                ", username='" + username + '\'' +
                '}';
    }
}
