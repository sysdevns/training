package uk.co.morrisonspls.sysdevns.logintrainingapplication.modules.login;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import uk.co.morrisonspls.sysdevns.logintrainingapplication.R;
import uk.co.morrisonspls.sysdevns.logintrainingapplication.modules.dashboard.DashboardActivity;

public class LoginActivity extends AppCompatActivity implements LoginPresenter.View {

    LoginPresenter loginPresenter;

    @BindView(R.id.pb_login_form)
    ProgressBar progressBar;
    @BindView(R.id.et_user)
    EditText et_user;
    @BindView(R.id.et_password)
    EditText et_password;
    @BindView(R.id.tv_error_msg)
    TextView tv_errorMsg;
    @BindView(R.id.sv_login_form)
    ScrollView scrollView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);

        loginPresenter = new LoginPresenter(this);
    }

    @OnClick(R.id.bt_log_in)
    void onLogonButtonClicked() {
        String user = et_user.getText().toString().trim();
        String password = et_password.getText().toString();
        loginPresenter.loginButtonPressed(user, password);
    }

    @Override
    public void showError(Messages errorMsg) {
        tv_errorMsg.setText(getErrorText(errorMsg));
        tv_errorMsg.setVisibility(View.VISIBLE);
    }

    private String getErrorText(Messages errorMsg) {
        String errorText = "";
        switch (errorMsg) {
            case BAD_RESPONSE:
                errorText = getString(R.string.login_bad_response);
                break;
            case CONNECTION_FAILED:
                errorText = getString(R.string.login_connection_failed);
                break;
            case INVALID_CREDENTIALS:
                errorText = getString(R.string.error_invalid_credentials);
                break;
            case MISSING_USERNAME:
                errorText = getString(R.string.login_missing_username);
                break;
        }
        return errorText;
    }

    @Override
    public void hideError() {
        tv_errorMsg.setVisibility(View.GONE);
    }

    @Override
    public void launchDashboard() {
        DashboardActivity.launchDashboard(this);
        finish();
    }

    @Override
    public void showLogonInProgress() {
        progressBar.setVisibility(View.VISIBLE);
        scrollView.setVisibility(View.INVISIBLE);
    }

    @Override
    public void hideLogonInProgress() {
        progressBar.setVisibility(View.GONE);
        scrollView.setVisibility(View.VISIBLE);
    }
}

