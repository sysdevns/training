package uk.co.morrisonspls.sysdevns.logintrainingapplication.manager;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import uk.co.morrisonspls.sysdevns.logintrainingapplication.BuildConfig;
import uk.co.morrisonspls.sysdevns.logintrainingapplication.api.LoginService;

public class ServiceGenerator {

    static LoginService provideLoginService() {
        return getRetrofitAdapter().create(LoginService.class);
    }

    private static Retrofit getRetrofitAdapter() {
        return new Retrofit.Builder()
                .baseUrl(BuildConfig.BASE_API_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }
}