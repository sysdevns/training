package uk.co.morrisonspls.sysdevns.logintrainingapplication.api;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;
import uk.co.morrisonspls.sysdevns.logintrainingapplication.model.LoginRequest;
import uk.co.morrisonspls.sysdevns.logintrainingapplication.model.User;

public interface LoginService {
    @POST("api/login")
    Call<User> login(@Body LoginRequest loginRequest);
}
