package uk.co.morrisonspls.sysdevns.logintrainingapplication.modules.dashboard;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import uk.co.morrisonspls.sysdevns.logintrainingapplication.R;

public class DashboardActivity extends AppCompatActivity {

    @BindView(R.id.tv_dashboard) TextView tvDashboard;

    public static void launchDashboard(Context context) {
        Intent intent = new Intent(context, DashboardActivity.class);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);
        ButterKnife.bind(this);
        tvDashboard.setText("Welcome to the dashboard");
    }
}
